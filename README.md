# Tetris in One Week aka TOW
## Baruchel Barnabé and Coatanhay-Meyer Evariste
### Installation
`git clone git@codeberg.org:Evrst/TOW.git`
### The Project
The goal of this Project is to recreate a Tetris game using C and SDL in just one week.

![Watch Gameplay Here !](https://codeberg.org/Evrst/TOW/src/branch/main/tetris.gif)
