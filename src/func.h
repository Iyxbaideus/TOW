#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

int choosePiece(int lastPiece);
void createPiece(int piece[2][4],int type);
int getStatistics(int statistics[7]);
int createGrid(int grid[20][10]);
int printGrid(int grid[20][10],SDL_Renderer* renderer,int level);
int gridAdd(int piece[2][4],int cursor[2],int angle,int grid[20][10],int type);
int printPiece(int piece[2][4],int cursor[2],int angle,SDL_Renderer* renderer,int type,int level);
int leftColliding(int piece[2][4],int angle,int cursor[2],int grid[20][10],int type);
int rightColliding(int piece[2][4],int angle,int cursor[2],int grid[20][10],int type);
int downColliding(int piece[2][4],int angle,int cursor[2],int grid[20][10],int type);
int isColliding(int piece[2][4],int angle,int cursor[2],int grid[20][10],int type);
int input(SDL_Event Event,int *running);
int shift(int cursor[2],int piece[2][4],int type,int angle,int grid[20][10],int currentFrame,float *gravity,int gravityValue,int *movingState,int *score);
int rotate(int piece[2][4],int type,int *angle,int grid[20][10],int cursor[2]);
int line(int grid[20][10],int *lineSum);
int printScore(int score,SDL_Renderer* renderer,SDL_Texture* text,SDL_Rect textRect,SDL_Texture* scoreText,SDL_Surface* *scoreSurface,SDL_Rect scoreRect,TTF_Font *textFont,SDL_Color blanc);
int printLevel(int level,SDL_Renderer* renderer,SDL_Texture* LEVEL,SDL_Rect LEVELRect,SDL_Texture* levelText,SDL_Surface* *levelSurface,SDL_Rect levelRect,TTF_Font *textFont,SDL_Color blanc);
int scoreFromLine(int nline);

